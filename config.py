config = {'brightness': 0.5,
          'white_black': 1.5}
FILTERS = ('bright', 'negative', 'white_black', 'gray', 'sepia', 'blur', 'mask')
DIRS = {'image_dir': './images',
        'out': './out'
    }
