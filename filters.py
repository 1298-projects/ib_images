from PIL import Image, ImageFilter, ImageEnhance
from config import config

class Filters():
    def __init__(self, type_filter, source_name, result_name, arq):
        self.arq = arq
        self.type_filter = type_filter
        filters = {'bright': self.bright,
                'negative': self.negative,
                'white_black': self.white_black,
                'gray': self.gray,
                'sepia': self.sepia,
                'blur': self.blur,
                'mask_color': self.mask_color,
                'mask_contrast': self.mask_contrast
                }
        filters.get(type_filter)(source_name, f'{result_name}')

    def bright(self, source_name, result_name):
        brightness = config.get('brightness')
        source = Image.open(source_name)
        result = ImageEnhance.Sharpness(source).enhance(0.0)
        result.save(result_name, "JPEG")

    def negative(self, source_name, result_name):
        source = Image.open(source_name)
        result = Image.new('RGB', source.size)
        for x in range(source.size[0]):
            for y in range(source.size[1]):
                r, g, b = source.getpixel((x, y))
                result.putpixel((x, y), (255 - r, 255 - g, 255 - b))
        result.save(result_name, "JPEG")

    def white_black(self, source_name, result_name):
        brightness = config.get('white_black')
        source = Image.open(source_name)
        result = Image.new('RGB', source.size)
        separator = 255 / brightness / 2 * 3
        for x in range(source.size[0]):
            for y in range(source.size[1]):
                r, g, b = source.getpixel((x, y))
                total = r + g + b
                if total > separator:
                    result.putpixel((x, y), (255, 255, 255))
                else:
                    result.putpixel((x, y), (0, 0, 0))
        result.save(result_name, "JPEG")

    def gray(self, source_name, result_name):
        source = Image.open(source_name)
        result = Image.new('RGB', source.size)
        for x in range(source.size[0]):
            for y in range(source.size[1]):
                r, g, b = source.getpixel((x, y))
                gray = int(r * 0.2126 + g * 0.7152 + b * 0.0722)
                result.putpixel((x, y), (gray, gray, gray))
        result.save(result_name, "JPEG")

    def sepia(self, source_name, result_name):
        source = Image.open(source_name)
        result = ImageEnhance.Brightness(source).enhance(self.arq)
        result.save(result_name, "JPEG")

    def blur(self, source_name, result_name):
        source = Image.open(source_name)
        result = Image.new('RGB', source.size)
        result = source.filter(ImageFilter.BLUR)
        result.save(result_name, "JPEG")

    def mask_color(self, source_name, result_name):
        source = Image.open(source_name)
        result = ImageEnhance.Color(source).enhance(self.arq)
        result.save(result_name, "JPEG")

    def mask_contrast(self, source_name, result_name):
        source = Image.open(source_name)
        result = ImageEnhance.Contrast(source).enhance(self.arq)
        result.save(result_name, "JPEG")
