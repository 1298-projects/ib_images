#!/usr/bin/env python3
import os
from sys import argv
from filters import Filters
from config import DIRS, FILTERS

def make_images(type_filter, arq=''):
    images = os.listdir(DIRS['image_dir'])
    for image in images:
        i = 0
        file_path = f'{DIRS["out"]}/{type_filter}_{i}_{image}'
        while os.path.exists(file_path):
            i += 1
            file_path = f'{DIRS["out"]}/{type_filter}_{i}_{image}'
        Filters(type_filter, f'{DIRS["image_dir"]}/{image}', f'{file_path}', arq)

def make_dirs():
    for name, path in DIRS.items():
        if not os.path.exists(path):
            os.mkdir(path)

def main():
    argument = argv[1:]
    if argument[0] == 'mask_color' or argument[0] == 'mask_contrast':
        arq = 5.0
        if len(argument) == 2:
            arq = float(argument[1])
        if argument[0] == 'mask_color':
            make_images('mask_color', arq)
        else:
            make_images('mask_contrast', arq)
    elif argument[0] == 'sepia' or argument[0] == 'bright':
        arq = 0.0
        if len(argument) == 2:
            arq = float(argument[1])
        if argument[0] == 'sepia':
            make_images('sepia', arq)
        else:
            make_images('bright', arq)
    else:
        make_images(argument[0])

if __name__ == '__main__':
    main()
