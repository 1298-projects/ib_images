# ib_images

Project for conference IB IT.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install requirements.

```bash
pip install -r requirements.txt
```

## Usage

```bash
main.py [-h] [-t FILTER]

ib_images

optional arguments:
  -h, --help  show this help message and exit
  -t FILTER   type of filters: bright, negative, white_black, gray, sepia,
              blur, mask_color, mask_contrast
```
